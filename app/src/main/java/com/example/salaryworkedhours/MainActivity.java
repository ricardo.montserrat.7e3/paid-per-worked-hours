package com.example.salaryworkedhours;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity
{
    Button seeResultButton;
    TextView resultText;
    EditText input;

    private void calculatePay(int hoursLimitBeforeExtra, int commonHoursPay)
    {
        int result, userValue, extraHours;

        userValue = Integer.parseInt(input.getText().toString());
        if (userValue > hoursLimitBeforeExtra)
        {
            extraHours = userValue - hoursLimitBeforeExtra;
            int extraHoursPay = 20;
            result = extraHours * extraHoursPay + hoursLimitBeforeExtra * commonHoursPay;
            resultText.setText(String.format(Locale.CANADA, "You worked %d hours and %d extra hours for a total of %dEUR (%dEUR an hour and %dEUR an extra hour).", hoursLimitBeforeExtra, extraHours, result, commonHoursPay, extraHoursPay));
        }
        else
        {
            result = userValue * commonHoursPay;
            resultText.setText(String.format(Locale.CANADA, "You worked %d hours for a total of %dEUR (%dEUR an hour).", userValue, result, commonHoursPay));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seeResultButton = (Button) findViewById(R.id.inputButton);
        resultText = (TextView) findViewById(R.id.resultText);
        input = (EditText) findViewById(R.id.input);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        resultText.setMovementMethod(new ScrollingMovementMethod());

        input.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66)
                    calculatePay(40,16);
                return false;
            }
        });

        seeResultButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                calculatePay(40,16);
            }
        });
    }
}